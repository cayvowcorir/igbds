@extends('partner::layouts.app')

@section('subtitle')
    Dashboard
@endsection

@section('contentheader_title')
    Dashboard
@endsection


@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-6">

                <!-- Default box -->
                <div class="box no-border">
                    <div class="box-header ">
                        <div class="pull-right">
                            <h5 class="  light-font pull-right">Emergency Alerts</h5>
                            <p class="dashboard-card-stat" style="">0</p>



                        </div>
                        <div class="box no-border box-overlay" style="background-color: limegreen">
                            <img style="width: 25px;" src="{{asset('icons/person-marker.png')}}"/>

                        </div>


                        {{--<div class="box-tools pull-right">--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--}}
                        {{--<i class="fa fa-minus"></i></button>--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">--}}
                        {{--<i class="fa fa-times"></i></button>--}}
                        {{--</div>--}}

                    </div>
                    <hr style="margin: 0px 15px;">
                    <div class="box-body">


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <div class="col-md-6">

                <!-- Default box -->
                <div class="box no-border">
                    <div class="box-header ">
                        <div class="pull-right">

                            <h5 class=" light-font pull-right">Routine Donation Requests</h5>


                            <p class="dashboard-card-stat" style="">0</p>


                        </div>


                        <div class="box no-border box-overlay" style="background-color: orange">

                            <img style="width: 30px;" src="{{asset('icons/donation-center-marker.png')}}"/>

                        </div>


                        {{--<div class="box-tools pull-right">--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--}}
                        {{--<i class="fa fa-minus"></i></button>--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">--}}
                        {{--<i class="fa fa-times"></i></button>--}}
                        {{--</div>--}}

                    </div>
                    <hr style="margin: 0px 15px;">
                    <div class="box-body">

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>

        </div>
        <div class="row">
            <div class="col-sm-12 ">



            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 ">

            </div>
        </div>
    </div>
    <div id="loading_spinner" style="position:fixed; top: 0; left:0; justify-content: center; display: none; background-color: rgb(0,0,0, 0.35);z-index: 9999; align-items: center; width: 100vw; height: 100vh">
        <div  class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
@endsection




