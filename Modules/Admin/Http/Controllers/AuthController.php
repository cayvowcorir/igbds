<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\UserDetail;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use JWTAuth;

class AuthController extends Controller
{
   public function login(Request $request){
       if($request->isMethod('GET')){
           return view('admin::auth.login');
       }
       else if($request->isMethod('POST')){
           if ($request->header('X-REQUESTED-BY')) {
               $user=Sentinel::authenticate(array('email'=>$request->email, 'password'=>$request->password));
               if($user){
                   $token=JWTAuth::fromUser($user);
                   return response()->json(['status' => 'success',  'user'=>$user, 'token'=>$token]);
               }
               else return response()->json(['status' => 'failed','message'=>'email/password mismatch',]);

           }
           else{
               $user=Sentinel::authenticate(['email'=>$request->email, 'password'=>$request->password]);
               if($user && Sentinel::inRole('admin')){
                   return redirect()->route('admin.dashboard');
               }
               else return redirect()->back()->withErrors(['message'=>"Invalid Credentials"]);
           }

       }
   }

    public function activate(Request $request)
    {
        if ($request->isMethod('GET')) {
            $email = Crypt::decrypt($request->hash);
            $user = Sentinel::findByCredentials(['email' => $email]);

            if (Activation::complete($user, $request->code)) {
                return redirect()->route('partner.password.set', ['uid' => $user->id]);
            } else {
                return redirect()->route('partner.login');
            }


        }
    }
    public function resetPassword(Request $request){
    if($request->isMethod('POST')){
            if ($request->header('X-REQUESTED-BY')) {
                $user = Sentinel::findByCredentials(array('email' => $request->email));
                if ($user) {
                    Sentinel::update($user, ['password'=>$request->password]);
                    return response()->json(['status' => 'success', 'user' => $user]);
                } else return response()->json(['status' => 'failed', 'message' => 'user non-existent',]);

            }

        }
    }

    public function register(Request $request){

       if($request->header('X-REQUESTED-BY')){
           $credentials=[
             'first_name'=>$request->first_name,
             'last_name'=>$request->last_name,
             'phone'=>$request->phone,
             'email'=>$request->email,
             'password'=>$request->password,
           ];
           try{
               $user = Sentinel::registerAndActivate($credentials);

               $token=JWTAuth::fromUser($user);
               $role = Sentinel::findRoleByName('Donor');
               $role->users()->attach($user);
               $userDetails=UserDetail::create([
                   'user_id'=>$user->id,
                   'blood_group'=>'',
               ]);
               return response()->json(['status' => 'success','message'=>'User registered successfully', 'user'=>$user, 'token'=>$token]);

           }
           catch (\Exception $e){
               return response()->json(['status' => 'failed','message'=>$e]);
           }

       }
   }
}


