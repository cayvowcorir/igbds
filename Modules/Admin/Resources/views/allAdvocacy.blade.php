@extends('admin::layouts.app')

@section('subtitle')
    Advocacy Content
@endsection

@section('contentheader_title')
    Advocacy Content
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-red ">
                    <div class="box-header with-border">
                        <div class="box-title-wrapper" >
                            <h3 class="box-title light-font">Advocacy Content</h3>
                        </div>
                        <div class="box-tools pull-right">

                            <a style="margin-right: 10px" href="javascript:window.location.reload(true)"><i class="fa fa-refresh fa-2x"></i></a>
                        </div>
                        <!-- /.box-tools -->

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <all-advocacy :advocacy-material="{{$advocacyMaterial}}"
                                      :advocacy-content-route="{{json_encode(route('admin.advocacy.index'))}}"
                        ></all-advocacy>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>

@endsection

<script>


</script>
