<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_locations', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->integer('user_id')->index('user_id')->unsigned();
            $table->float('lat',10)->nullable();
            $table->float('lng',10)->nullable();
            $table->timestamps();
        });

        Schema::create('partners', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->string('name',30);
            $table->text('description')->nullable();
            $table->string('company_reg_cert_number',30);
            $table->string('company_reg_cert_url',100)->nullable();
            $table->integer('contact_person_id')->index('contact_person_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::create('advocacy_materials', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->string('name',60);
            $table->text('description')->nullable();
            $table->text('attachment_url');
            $table->text('preview_url');
            $table->integer('author_id')->index('author_id')->unsigned();
            $table->timestamps();
        });
        Schema::create('feedback', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->string('subject',60);
            $table->string('short_description',60);
            $table->text('detailed_description')->nullable();
            $table->integer('user_id')->index('user_id')->unsigned();
            $table->timestamps();
        });
        Schema::create('donation_reminders', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->string('title',60);
            $table->string('content',60);
            $table->integer('user_id')->index('user_id')->unsigned();
            $table->timestamps();
        });
        Schema::create('forum_messages', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->integer('user_id')->index('user_id')->unsigned();
            $table->text('message')->nullable();
            $table->timestamps();
        });
        Schema::create('chat_messages', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->integer('user_id')->index('user_id')->unsigned();
            $table->string('subject',100)->nullable();
            $table->text('message')->nullable();
            $table->enum('status', ['SENT', 'RECEIVED', 'SEEN', 'UNSENT']);
            $table->timestamps();
        });
        Schema::create('donation_centers', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();

            $table->string('name',60);
            $table->text('description')->nullable();
            $table->string('physical_address',60)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('phone', 50)->nullable();
            $table->timestamps();
        });
        Schema::create('donation_center_locations', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->string('name',60);
            $table->integer('donation_center_id')->index('donation_center_id')->unsigned();
            $table->float('lat',10)->nullable();
            $table->float('lng',10)->nullable();
            $table->timestamps();
        });
        Schema::create('user_details', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->string('blood_group',30);
            $table->integer('user_id')->index('user_id')->unsigned();
            $table->timestamps();
        });
        Schema::create('blood_donations', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->integer('user_id')->index('user_id')->unsigned();
            $table->date('donation_date');
            $table->enum('disbursement_status', ['PENDING','DISBURSED']);
            $table->integer('donation_center_id')->index('donation_center_id')->unsigned();
            $table->timestamps();
        });
        Schema::create('workflows', function(Blueprint $table)
        {
            $table->integer('id', true)->unsigned();
            $table->integer('partner_id')->index('partner_id')->unsigned();
            $table->string('name',50);
            $table->text('description');
            $table->enum('status', ['PENDING','APPROVED', 'REJECTED']);
            $table->date('initiation_date');
            $table->date('completion_date');
            $table->integer('approver_id')->index('approver_id')->unsigned();
            $table->timestamps();
        });

//        Foreign Keys

        Schema::table('user_locations', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
        Schema::table('partners', function(Blueprint $table)
        {
            $table->foreign('contact_person_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
        Schema::table('advocacy_materials', function(Blueprint $table)
        {
            $table->foreign('author_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
        Schema::table('feedback', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
        Schema::table('donation_reminders', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
        Schema::table('forum_messages', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
        Schema::table('chat_messages', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
        Schema::table('donation_center_locations', function(Blueprint $table)
        {
            $table->foreign('donation_center_id')->references('id')->on('donation_centers')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
        Schema::table('user_details', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
        Schema::table('blood_donations', function(Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('donation_center_id')->references('id')->on('donation_centers')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
        Schema::table('workflows', function(Blueprint $table)
        {
            $table->foreign('partner_id')->references('id')->on('partners')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('approver_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('workflows');
        Schema::drop('blood_donations');
        Schema::drop('user_details');
        Schema::drop('donation_center_locations');
        Schema::drop('donation_centers');
        Schema::drop('chat_messages');
        Schema::drop('forum_messages');
        Schema::drop('donation_reminders');
        Schema::drop('feedback');
        Schema::drop('advocacy_materials');
        Schema::drop('partners');
        Schema::drop('user_locations');
    }
}
