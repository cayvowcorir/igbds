@extends('admin::layouts.app')

@section('subtitle')
    Surveys
@endsection

@section('contentheader_title')
    Surveys
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-red ">
                    <div class="box-header with-border">
                        <div class="box-title-wrapper" >
                            <h3 class="box-title light-font">Surveys</h3>
                        </div>
                        <div class="box-tools pull-right">

                            <a class="trigger-custom btn  btn-flat bordered-button button-transparent "
                               href="{{route('admin.surveys.add')}}"><i
                                        class="fa fa-plus"></i> Add A Survey
                            </a>
                        </div>
                        <!-- /.box-tools -->

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <surveys :all-surveys="{{json_encode($surveys)}}"></surveys>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>

@endsection

<script>


</script>
