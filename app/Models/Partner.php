<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $fillable=[
        'name',
        'description',
        'company_reg_cert_number',
        'phone',
        'company_email',
        'company_website',
        'postal_address',
        'physical_address',
    ];
}
