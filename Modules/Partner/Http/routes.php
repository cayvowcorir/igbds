<?php

Route::group(['middleware' => 'web', 'prefix' => 'partner', 'namespace' => 'Modules\Partner\Http\Controllers'], function()
{
    Route::get('/', 'AuthController@login')->name('partner.welcome');
    Route::match(['GET', 'POST'],'/auth/login', 'AuthController@login')->name('partner.login');
    Route::match(['GET', 'POST'],'/auth/password/{uid}/set', 'AuthController@setPassword')->name('partner.password.set');
    Route::group(['middleware'=>'checkAuth'], function(){
        Route::get('/dashboard', 'PartnerController@dashboard')->name('partner.dashboard');
        Route::match(['GET', 'POST'],'/donations/add', 'PartnerController@recordDonation')->name('partner.donations.add');
        Route::match(['GET', 'POST'],'/emergency/add', 'PartnerController@addEmergency')->name('partner.emergency.add');
        Route::get('/partner/donors/search', 'PartnerController@recordDonation')->name('partner.donations.add');
        Route::get('/search', 'PartnerController@donorSearch')->name('partner.donor.search');
    });
});
