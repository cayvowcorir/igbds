<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Config;
class jwt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            // Config::set('auth.model', \App\Models\Customer::class);
            $user = JWTAuth::parseToken()->authenticate();
            $request->request->add(['user' => $user]);

        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json([
                    'code'    => 401,
                    'message' => 'Unauthorised',
                    'error'   => 'Token is Invalid',
                ]);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json([
                    'code'    => 401,
                    'message' => 'Unauthorised',
                    'error'   => 'Token is Expired',
                ]);
            } else {
                return response()->json([
                    'code'    => 401,
                    'message' => 'Unauthorised',
                    'error'   => 'Something went wrong']);
            }
        }
        return $next($request);

    }
}
