@extends('partner::layouts.app')

@section('subtitle')
    Record Donation
@endsection

@section('contentheader_title')
    Record Donation
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-red ">
                    <!-- /.box-header -->
                    <div class="box-title-wrapper" >
                        <h3 class="box-title light-font">Record new Donation</h3>
                    </div>
                    <div class="box-body">
                        <record-donation :donation-centers="{{$donationCenters}}" :donor-search-url="{{json_encode(route('partner.donor.search'))}}"></record-donation>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>

@endsection

<script>


</script>
