<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\BloodDonation;
use App\Models\DonationCenter;
use App\Models\DonationCenterLocation;
use App\Models\UserDetail;
use App\Models\UserLocation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use PHPUnit\Util\RegularExpressionTest;
use Yajra\DataTables\Facades\DataTables;
use Debugbar;

class DonationCenterController extends Controller
{
    public function addDonationCenter(Request $request){
        if($request->isMethod('GET')){
            return view('admin::addDonationCenter');
        }
        else if ($request->isMethod('POST')){
            $donationCenter=DonationCenter::create([
                'name'=>$request->name,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'description'=>$request->description,
                'physical_address'=>$request->physical_address,
            ]);
            DonationCenterLocation::create([
                'name'=>$request->name,
                'donation_center_id'=>$donationCenter->id,
                'lat'=>$request->lat,
                'lng'=>$request->lng,
            ]);
            return \response()->json(['status'=>'success', "message"=>""]);
        }
    }
    public function donationCenters(Request $request)
    {
        if($request->isMethod('GET')){
            if($request->header('X-REQUESTED-BY')=='API'){
                $donationCenters=DonationCenterLocation::with('donationCenter')->get();
                return \response()->json(['status'=>'success', "message"=>"", "donationCenters"=>$donationCenters]);
            }
            else{
                return view('admin::allDonationCenters');

            }
        }

    }
    public function donationCentersData(Request $request)
    {
        return DataTables::of(DonationCenter::all())->addColumn('action', function ($center) {
            return '<a href="' . route('admin.donation_centers.view', $center->id) .'">View</a>';
        })
            ->rawColumns(['action'])->make(true);
    }

    public function viewDonationCenter(Request $request)
    {
        if ($request->isMethod('GET')) {
            $donationCenter= DonationCenter::where('id',$request->id)->with('donationCenterLocation')->first();
            $aggregatedBloodDonations=DB::table('blood_donations')
                ->select(DB::raw('MONTH(donation_date) month'), DB::raw('count(*) as total'))
                ->where('donation_center_id',$request->id)->whereYear('donation_date', '=',date('Y') )
                ->groupBy('month')
                ->get();
            $allDonations=BloodDonation::where('donation_center_id', $request->id)->get();
            $disbursedDonations=0;
            foreach ($allDonations as $donation){
                if($donation->disbursement_status=='DISBURSED'){
                    $disbursedDonations++;
                }
            }
            if($request->header('X-REQUESTED-BY')=='API'){
                return \response()->json(['status' => 'success', "message" => "", 'donationCenter'=>$donationCenter]);
            }
            else{
                return view('admin::viewDonationCenter', ['donationCenter'=>$donationCenter,
                    'aggregatedBloodDonations'=>$aggregatedBloodDonations, 'allDonations'=>$allDonations, 'disbursedDonations'=>$disbursedDonations]);

            }

        }

    }
}
