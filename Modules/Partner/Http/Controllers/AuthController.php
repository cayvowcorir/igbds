<?php

namespace Modules\Partner\Http\Controllers;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use JWTAuth;

class AuthController extends Controller
{
   public function login(Request $request){
       if($request->isMethod('GET')){
           return view('partner::auth.login');
       }
       else if($request->isMethod('POST')){

               $user=Sentinel::authenticate(['email'=>$request->email, 'password'=>$request->password]);
               if($user && Sentinel::inRole('partner')){
                   return redirect()->route('partner.dashboard');
               }
               else return redirect()->back()->withErrors(['message'=>"Invalid Credentials"]);


       }
   }

    public function setPassword(Request $request)
    {
        if ($request->isMethod('GET')) {
            $user=Sentinel::findById($request->uid);
            return view('partner::auth.setPassword', ['email'=>$user->email]);
        }
        else if ($request->isMethod('POST')) {
            $user = Sentinel::findByCredentials(['email' => $request->email]);
            if(Sentinel::update($user,['password'=>$request->password])){
                return \response()->json(['status' => 'success', "message" => ""]);
            }
            else{
                return \response()->json(['status' => 'failed', "message" => ""]);
            }
        }

    }

    public function register(Request $request){

       if($request->header('X-REQUESTED-BY')){
           $credentials=[
             'first_name'=>$request->first_name,
             'last_name'=>$request->last_name,
             'phone'=>$request->phone,
             'email'=>$request->email,
             'password'=>$request->password,
           ];
           try{
               $user = Sentinel::registerAndActivate($credentials);

               $token=JWTAuth::fromUser($user);
               $role = Sentinel::findRoleByName('Donor');
               $role->users()->attach($user);
               return response()->json(['status' => 'success','message'=>'User registered successfully', 'user'=>$user, 'token'=>$token]);

           }
           catch (\Exception $e){
               return response()->json(['status' => 'failed','message'=>$e]);
           }

       }
   }
}


