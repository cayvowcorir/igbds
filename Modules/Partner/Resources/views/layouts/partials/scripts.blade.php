<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->



<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

<script src="{{asset('js/partner-module/main.js')}}" type="text/javascript"></script>
<script src="{{asset('js/datatables.min.js')}}"></script>


{{--<script src="{{ asset('js/app.js')}}" type="text/javascript"></script>--}}


<script>
            @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>

<script>
    function preview(which) {
        file = document.getElementById(which).files[0];
        file_url = URL.createObjectURL(file);
        $('#'+which+'-preview').attr('src', file_url);
    }

    $(document).ready(function() {
        @stack('jquery-scripts')

    });


</script>
<script>
    function initMap() {
        var kenya = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('map'), {

            zoom: 5,
            center: { lat:-0.023559,
                lng: 37.906193,}
        });
        var marker = new google.maps.Marker({
            position: kenya,
            map: map
        });
    }
</script>
{{--<script async defer--}}
        {{--src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap">--}}
{{--</script>--}}


