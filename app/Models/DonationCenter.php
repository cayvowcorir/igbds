<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DonationCenter extends Model
{
    protected $fillable=[
        'name',
        'description',
        'physical_address',
        'email',
        'phone',
    ];
    public function donationCenterLocation(){
        return($this->hasOne('App\Models\DonationCenterLocation'));
    }
}
