<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners', function (Blueprint $table) {
            $table->string('phone', 30);
            $table->text('physical_address');
            $table->text('postal_address');
            $table->string('company_email', 50);
            $table->string('company_website', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('physical_address');
            $table->dropColumn('postal_address');
            $table->dropColumn('company_email');
            $table->dropColumn('company_website');
        });
    }
}
