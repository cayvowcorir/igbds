<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BloodDonation extends Model
{
    protected $fillable=[
      'user_id',
        'donation_date',
        'disbursement_status',
        'donation_center_id',
    ];
    public function donationCenter(){
        return $this->belongsTo('App\Models\DonationCenter');
    }
}
