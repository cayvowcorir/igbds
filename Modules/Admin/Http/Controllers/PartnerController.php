<?php

namespace Modules\Admin\Http\Controllers;

use App\Mail\PartnerAccountCreated;
use App\Models\DonationCenter;
use App\Models\DonationCenterLocation;
use App\Models\Partner;
use App\Models\UserDetail;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;
use Debugbar;

class PartnerController extends Controller
{
    public function partners(Request $request){
        if($request->isMethod('GET')){
            return view('admin::allPartners');
        }
        else if ($request->isMethod('POST')){

        }

    }
    public function addPartner(Request $request){
        if($request->isMethod('GET')){
            return view('admin::addPartner');
        }
        else if ($request->isMethod('POST')){
            $partner=Partner::create([
                'name'=>$request->name,
                'description'=>$request->description,
                'company_reg_cert_number'=>$request->company_reg_cert_number,
                'phone'=>$request->phone,
                'company_email'=>$request->company_email,
                'company_website'=>$request->company_website,
                'postal_address'=>$request->postal_address,
                'physical_address'=>$request->physical_address,
            ]);
            $user=Sentinel::register([
                'first_name'=>$request->name,
                'last_name'=>$request->name,
                'phone'=>$request->phone,
                'email'=>$request->company_email,
                'password'=>'password',
            ]);
            $role = Sentinel::findRoleByName('Partner');

            $role->users()->attach($user);

            $hash = urlencode(Crypt::encrypt(["email"=>$user->email]));
            $activation=Activation::create($user);
            try{
                Mail::to($request->company_email)->queue(new PartnerAccountCreated($activation, $hash));
            }
            catch (\Exception $e){
                return \response()->json(['status'=>'failed', "message"=>$e->getMessage()]);
            }

            return \response()->json(['status'=>'success', "message"=>""]);
        }

    }
    public function partnersData(Request $request)
    {
        return DataTables::of(Partner::all())->make(true);
    }
}
