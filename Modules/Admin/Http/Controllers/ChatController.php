<?php

namespace Modules\Admin\Http\Controllers;

use App\Events\NewChatMessage;
use App\Events\NewMessage;
use App\Models\BloodDonation;
use App\Models\ChatMessage;
use App\Models\DonationCenter;
use App\Models\DonationCenterLocation;
use App\Models\FcmToken;
use App\Models\Feedback;
use App\Models\Sentinel\SentinelUserModel;
use App\Models\UserDetail;
use App\Models\UserLocation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use PhpParser\Node\Stmt\TryCatch;
use PHPUnit\Util\RegularExpressionTest;
use Yajra\DataTables\Facades\DataTables;
use Debugbar;
use App\Events\DonorLocationUpdated;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class ChatController extends Controller
{
    public function sendMessage(Request $request)
    {
        if ($request->isMethod('GET')) {

        }
        else if ($request->isMethod('POST')) {
            try{
                $chatMessage=ChatMessage::create([
                    'subject'=>$request->subject,
                    'message'=>$request->message,
                    'status'=>'SENT',
                    'user_id'=>$request->user->id
                ]);
                $userDetails=Sentinel::findById($request->user->id);
                broadcast(new NewChatMessage(['subject'=>$request->subject, 'message'=>$request->message,
                    'email'=>$userDetails->email, 'time'=>$chatMessage->created_at]));

                return \response()->json(['status' => 'success', "message" => ""]);
            }
            catch (\Exception $e){
                return \response()->json(['status' => 'failed', "message" => $e]);
            }
        }

    }
    public function chat(Request $request)
    {
        if ($request->isMethod('GET')) {
            $message=ChatMessage::find($request->id);
            $sentMessages=ChatMessage::with('sender')->where(['recipient_id'=> $message->user_id, 'user_id'=>$message->recipient_id])->get();
            $recievedMessages=ChatMessage::with('sender')->where(['recipient_id'=>$message->recipient_id, 'user_id'=>$message->user_id])->get();
            $chatMessages=[];
            $recipientDetails=SentinelUserModel::with('donorDetails')->where('id',$message->user_id)->first();
            foreach ($sentMessages as $sentMessage){
                $sentMessage->type="sent";
                array_push($chatMessages, $sentMessage);
            }
            foreach ($recievedMessages as $recievedMessage){
                $recievedMessage->type="recieved";
                array_push($chatMessages, $recievedMessage);
            }
            $sortedByDate=collect($chatMessages)->sortBy('created_at');
            $bloodDonations=BloodDonation::where('user_id', $message->user_id)->with('donationCenter')->get();
            $uniqueDonations=BloodDonation::where('user_id', $message->user_id)->groupBy('donation_center_id')->count();
            $bloodGroup=UserDetail::where('user_id',$message->user_id)->first();
            return view('admin::viewMessage', ['messages'=>$sortedByDate->values()->all(), 'recipientDetails'=>$recipientDetails,
                'bloodDonations'=>$bloodDonations,
                'uniqueDonations'=>$uniqueDonations,
                'bloodGroup'=>$bloodGroup,
                ]);
        }
        else if ($request->isMethod('POST')) {
            try{
                $thread=ChatMessage::find($request->id);
                $message=ChatMessage::create([
                    'message'=>$request->message,
                    'status'=>'SENT',
                    'user_id'=>$thread->recipient_id,
                    'recipient_id'=>$thread->user_id,
                ]);
                $sentMessage=ChatMessage::with('sender')->find($message->id);
                $sentMessage->type="sent";

                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60*20);

                $notificationBuilder = new PayloadNotificationBuilder('IGBDS - New Message');
                $notificationBuilder->setBody('You have received a new message from')
                    ->setSound('default');

                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['sender_id' => $sentMessage->sender->id]);

                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();

                $tokenRecord=FcmToken::where('user_id', $thread->user_id)->first();
                $token = $tokenRecord->fcm_token;

                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

                $downstreamResponse->numberSuccess();
                $downstreamResponse->numberFailure();
                $downstreamResponse->numberModification();

//return Array - you must remove all this tokens in your database
                $downstreamResponse->tokensToDelete();

//return Array (key : oldToken, value : new token - you must change the token in your database )
                $downstreamResponse->tokensToModify();

//return Array - you should try to resend the message to the tokens in the array
                $downstreamResponse->tokensToRetry();

// return Array (key:token, value:errror) - in production you should remove from your database the tokens
                return \response()->json(['status' => 'success', "message" => $sentMessage]);
            }
            catch (\Exception $e){
                dd($e);
                return \response()->json(['status' => 'failed', "message" => $e]);
            }

        }


    }

    public function chatMessages(Request $request)
    {
        if ($request->isMethod('GET')) {
            $sentMessages=ChatMessage::where(['user_id'=> $request->user->id, 'recipient_id'=>$request->id])->get();
            $recievedMessages=ChatMessage::where(['user_id'=> $request->id, 'recipient_id'=>$request->user->id])->get();
            $chatMessages=[];
            foreach ($sentMessages as $sentMessage){
                $sentMessage->type="sent";
                array_push($chatMessages, $sentMessage);
            }
            foreach ($recievedMessages as $recievedMessage){
                $recievedMessage->type="received";
                array_push($chatMessages, $recievedMessage);
            }
            $sortedByDate=collect($chatMessages)->sortBy('count')->toArray();
            return \response()->json(['status' => 'success', "message" => "", 'chatMessages'=>$sortedByDate]);
        }
        else if ($request->isMethod('POST')) {
            try{
                $chatMessage=ChatMessage::create([
                    'user_id'=>$request->user->id,
                    'recipient_id'=>$request->id,
                    'message'=>$request->message,
                    'status'=>'SENT',
                ]);
                $recipient=Sentinel::findById($request->id);
                $userDetails=Sentinel::findById($request->user->id);
                $messageDetails=[
                    'id'=>$chatMessage->id,
                    'message'=>$request->message,
                    'sender'=>['first_name'=>$userDetails->first_name,'last_name'=>$userDetails->last_name, 'email'=>$userDetails->email,],
                    'recipient'=>['recipient_id'=>$recipient->id,'first_name'=>$userDetails->first_name,
                       'last_name'=>$userDetails->last_name],
                    'date'=>$chatMessage->created_at];
                broadcast(new NewChatMessage($messageDetails));
                return \response()->json(['status' => 'success', "message" => "Message Sent Successfully"]);
            }
            catch (\Exception $e){
                return \response()->json(['status' => 'failed', "message" => $e]);
            }

        }

    }


    public function getUserDetails(Request $request)
    {
        if ($request->isMethod('GET')) {
            $userDetails=Sentinel::findById($request->id);
            return \response()->json(['status' => 'success', "message" => "", 'userDetails'=>$userDetails]);
        }

    }
}
