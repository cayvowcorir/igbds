@extends('admin::layouts.app')

@section('subtitle')
    Donation Centers
@endsection

@section('contentheader_title')
    Donation Centers
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-red ">
                    <div class="box-header with-border">
                        <div class="box-title-wrapper" >
                            <h3 class="box-title light-font">Donation Centers</h3>
                        </div>
                        <div class="box-tools pull-right">
                            <a class="trigger-custom btn  btn-flat bordered-button button-transparent "
                                    href="{{route('admin.donation_centers.add')}}"><i
                                        class="fa fa-plus"></i> Add Donation Center
                            </a>
                        </div>
                        <!-- /.box-tools -->

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-responsive" style="width: 100%; font-size: 14px;" id="donation_centers_datatable">
                            <thead>
                            <tr>
                                <th >Name</th>
                                <th>Description</th>
                                <th>Physical Address</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>

@endsection

<script>

    @push('jquery-scripts')

      $('#donation_centers_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.donation_centers.data') !!}',
        columns: [
            { data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'physical_address', name: 'physical_address'},
            {data: 'action', name: 'action'},
        ]
    });
    @endpush
</script>
