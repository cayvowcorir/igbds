<?php

namespace Modules\Partner\Http\Controllers;

use App\Models\BloodDonation;
use App\Models\DonationCenter;
use App\Models\Sentinel\SentinelUserModel;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class PartnerController extends Controller
{
    public function dashboard(){

        return view('partner::dashboard');
    }

    public function recordDonation(Request $request)
    {
        if ($request->isMethod('GET')) {
            $donationCenters=DonationCenter::select(['id', 'name'])->get();
            return view('partner::recordDonation', ['donationCenters'=>$donationCenters]);
        }
        else if ($request->isMethod('POST')) {
            try{
                BloodDonation::create([
                    'user_id'=>$request->user_id,
                    'donation_date'=>date('Y-m-d', strtotime($request->date)),
                    'disbursement_status'=>'PENDING',
                    'donation_center_id'=>$request->center,
                ]);
                try{
                    $donorDetails=UserDetail::where('user_id', $request->user_id)->first();
                    if(!$donorDetails){
                        UserDetail::create([
                            'user_id'=>$request->user_id,
                            'blood_group'=>$request->blood_group,
                        ]);
                    }
                    else{
                        UserDetail::where('user_id', $request->user_id)->update(['blood_group'=>$request->blood_group]);
                    }
                }
                catch (\Exception $e){
                    return \response()->json(['status' => 'failed', "message" => $e->getMessage()]);

                }

                return \response()->json(['status' => 'success', "message" => ""]);

            }
            catch (\Exception $e){
                return \response()->json(['status' => 'failed', "message" => $e->getMessage()]);
            }
        }

    }

    public function addEmergency(Request $request)
    {
        if ($request->isMethod('GET')) {
            $donationCenters=DonationCenter::select(['id', 'name'])->get();
            return view('partner::addEmergency',['donationCenters'=>$donationCenters]);
        }

    }

    public function donorSearch(Request $request)
    {
        if ($request->isMethod('GET')) {

            $results = SentinelUserModel::search($request->q)->get();

            return response()->json($results);
        }


    }
}
