<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\AdvocacyMaterial;
use App\Models\BloodDonation;
use App\Models\DonationCenter;
use App\Models\DonationCenterLocation;
use App\Models\UserDetail;
use App\Models\UserLocation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Util\RegularExpressionTest;
use Yajra\DataTables\Facades\DataTables;
use Debugbar;
use App\Events\DonorLocationUpdated;

class AdvocacyMaterialController extends Controller
{
    public function advocacyMaterial(Request $request)
    {
        if ($request->isMethod('GET')) {
            $advocacyMaterial=AdvocacyMaterial::all();
            if($request->header('X-REQUESTED-BY')=='API'){
                return \response()->json(['status' => 'success', "message" => "", 'advocacyMaterial'=>$advocacyMaterial]);
            }
            else{

                return view('admin::allAdvocacy', ['advocacyMaterial'=>$advocacyMaterial]);
            }

        }
        else if($request->isMethod('POST')){
            $file = $request->file('file');
            try{
                $name=$file->storeAs('advocacy_material','advocacy_material_'.$file->getClientOriginalName());
                if($file->getClientMimeType()=='image/*'){
                    $previewUrl=$name;
                }
                else{
                    $previewUrl='preview/placeholder.png';
                }
                Storage::setVisibility($name, 'public');
                AdvocacyMaterial::create([
                    'name'=>$file->getClientOriginalName(),
                    'attachment_url'=>$name,
                    'preview_url'=>$previewUrl,
                    'author_id'=>Sentinel::getUser()->id
                ]);
                return \response()->json(['status' => 'success', "message" => ""]);
            }
            catch (\Exception $e){
                return \response()->json(['status' => 'failed', "message" => $e], 419);
            }

//
        }

    }

    public function downloadAttachment(Request $request)
    {
        if ($request->isMethod('GET')) {
            $headers = array('Content-Type: image/png');
            return \response()->download(storage_path('app/advocacy_material/'.$request->fileName));
        }

    }
    public function previewAttachment(Request $request)
    {
        if ($request->isMethod('GET')) {
            return Storage::get('advocacy_material/'.$request->fileName);
        }

    }
}
