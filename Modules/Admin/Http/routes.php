<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    Route::match(['GET', 'POST'],'/auth/login', 'AuthController@login')->name('admin.login');

    //Partner Activation
 
	Route::get('/', 'AuthController@login')->name('admin.home');
   Route::get('/partners/account/activate', 'AuthController@activate')->name('admin.partners.activate');

    Route::group(['middleware'=>'checkAuth'], function(){
        Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');

        /*Donation Centers*/
        Route::get('/donation-centers', 'DonationCenterController@donationCenters')->name('admin.donation_centers.index');
        Route::get('/donation-centers/view/{id}', 'DonationCenterController@viewDonationCenter')->name('admin.donation_centers.view');
        Route::match(['POST', 'GET'],'/donation-centers/add', 'DonationCenterController@addDonationCenter')->name('admin.donation_centers.add');
        Route::get('/donation_centers.data', 'DonationCenterController@donationCentersData')->name('admin.donation_centers.data');


        /*Donors*/
        Route::match(['POST', 'GET'],'/donors', 'DonorController@donors')->name('admin.donors.index');
        Route::match(['POST', 'GET'],'/donors/view/{id}', 'DonorController@viewDonor')->name('admin.donors.view');
        Route::get('/donors.data', 'DonorController@donorsData')->name('admin.donors.data');

        /*Partners*/
        Route::get('/partners', 'PartnerController@partners')->name('admin.partners.index');
        Route::match(['POST', 'GET'],'/partners/add', 'PartnerController@addPartner')->name('admin.partners.add');
        Route::get('/partners.data', 'PartnerController@partnersData')->name('admin.partners.data');

        /*Advocacy Content*/
        Route::match(['POST', 'GET'],'/advocacy-material', 'AdvocacyMaterialController@advocacyMaterial')->name('admin.advocacy.index');
        Route::get('/preview/{fileName}', 'AdvocacyMaterialController@previewAttachment')->name('admin.advocacy.material');
        Route::get('/advocacy_material/{fileName}', 'AdvocacyMaterialController@downloadAttachment')->name('admin.advocacy.download');
        Route::match(['POST', 'GET'],'/partners/add', 'PartnerController@addPartner')->name('admin.partners.add');
        Route::get('/partners.data', 'PartnerController@partnersData')->name('admin.partners.data');

        /*Messages*/
        Route::get('/messages', 'AdminController@messages')->name('admin.messages.index');
        Route::match(['POST', 'GET'],'/messages/{id}', 'ChatController@chat')->name('admin.messages.chat');

        /*Surveys*/
        Route::get('/surveys', 'SurveyController@allSurveys')->name('admin.surveys.index');
        Route::match(['GET','POST'],'/surveys/add', 'SurveyController@addSurvey')->name('admin.surveys.add');
        Route::post('/surveys/{id}/question-groups/add', 'SurveyController@addSurveyQuestionGroup')->name('admin.surveys.group.add');
        Route::post('/surveys/{id}/questions/add', 'SurveyController@addSurveyQuestions')->name('admin.surveys.questions.add');
    });


});
