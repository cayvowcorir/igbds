<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table='feedback';
    protected $fillable=[
        'subject',
        'short_description',
        'detailed_description',
        'user_id'
    ];
}
