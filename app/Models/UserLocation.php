<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model
{
    protected $fillable=[
      'user_id',
        'lat',
        'lng'
    ];
    public function user(){
        return $this->belongsTo('Cartalyst\Sentinel\Users\EloquentUser', 'user_id');
    }
}
