@extends('admin::layouts.app')

@section('subtitle')
    Partners
@endsection

@section('contentheader_title')
    Partners
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-red ">
                    <div class="box-header with-border">
                        <div class="box-title-wrapper" >
                            <h3 class="box-title light-font">Partners</h3>
                        </div>
                        <div class="box-tools pull-right">

                            <a class="trigger-custom btn  btn-flat bordered-button button-transparent "
                               href="{{route('admin.partners.add')}}"><i
                                        class="fa fa-plus"></i> Add Partner
                            </a>
                        </div>
                        <!-- /.box-tools -->

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-responsive" style="width: 100%; font-size: 14px;" id="partners-datatable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Email</th>
                                <th>Company Phone</th>
                                <th>Postal Address</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
    <div class="modal-custom"></div>

@endsection

<script>

    @push('jquery-scripts')

      $('#partners-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.partners.data') !!}',
        columns: [
            { data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'company_email', name: 'company_email'},
            {data: 'phone', name: 'phone'},
            {data: 'postal_address', name: 'postal_address'},
        ]
    });
    @endpush
</script>
