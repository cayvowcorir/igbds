<?php

namespace Modules\Admin\Http\Controllers;

use App\Models\ChatMessage;
use App\Models\DonationCenter;
use App\Models\DonationCenterLocation;
use App\Models\Feedback;
use App\Models\Sentinel\SentinelUserModel;
use App\Models\UserLocation;
use Cartalyst\Sentinel\Users\EloquentUser;
use Debugbar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function dashboard()
    {
        $donationCenters = DonationCenter::with('donationCenterLocation')->get();
        $donorLocations = UserLocation::with('user')->get();
        $usersCount = EloquentUser::with('roles')->whereHas('roles', function ($query) {
            $query->where('name', '=', 'Donor');
        })->count();

        return view('admin::dashboard', ['donationCenters' => $donationCenters, 'donorLocations' => $donorLocations, 'usersCount' => $usersCount
        ]);
    }

    public function allUsers(Request $request)
    {
        if ($request->isMethod('GET')) {
            $allUsers = SentinelUserModel::select(['id', 'first_name', 'image_url', 'last_name', 'email'])
                ->whereNotIn('id', [$request->user->id])->get();
            return \response()->json(['status' => 'success', "message" => "", "users" => $allUsers]);
        }

    }

    public function messages(Request $request)
    {
        if ($request->isMethod('GET')) {
            $messages = ChatMessage::with(['recipient', 'sender'])->where('recipient_id', 1)->get();
            return \response()->json(['status' => 'success', "messages" => $messages]);
        }

    }



}
