<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar sidebar-overlay"
       style="position: fixed;background-image: url({{asset('img/sidebar_bg.jpg')}})">
    <hr style="margin:10px 5px 5px 20px; position: relative; ">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if ($user=Sentinel::check())
            <div class="user-panel" style="margin-left: 10px;">
                <div class="pull-left image">

                    <img src=" {{ asset('img/avatar5.png') }}" class="img-circle" alt="Your Logo"/>

                </div>
                <div class="pull-left info">
                    <p>{{ $user->first_name }}</p>
                {{--<p>User {{$user}}</p>--}}
                <!-- Status -->
                    <a href="#" style=" color: white!important; font-size: 14px"><i
                                class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
        @endif
        <hr style="margin:10px 5px 5px 20px; position: relative; ">

        <!-- search form (Optional) -->

        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">

            <!-- Optionally, you can add icons to the links -->
            <li class="{{preg_match('\'admin.dashboard\'',Route::current()->getName())==1 ?'active':''}}"><a
                        href="{{ route('admin.dashboard') }}"><i class='ti-dashboard'></i> <span>Dashboard</span></a>
            </li>
            <li class="{{preg_match('\'admin.donors.\'',Route::current()->getName())==1 ?'active':''}}"><a
                        href="{{ route('admin.donors.index') }}"><i class='ti-id-badge'></i> <span>Donors</span></a>
            </li>

            <li class="treeview {{preg_match('\'admin.donation_centers.\'',Route::current()->getName())==1 ?'active':''}}">
                <a href="#"><i class='ti-pulse'></i> <span>Donation Centers</span><i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.donation_centers.add') }}"><i class='fa fa-plus'></i> <span>Add Donation Center</span></a>
                    </li>
                    <li><a href="{{ route('admin.donation_centers.index') }}"><i class='fa fa-list'></i> <span>All Donation Centers</span></a>
                    </li>
                </ul>
            </li>
            <li class="{{preg_match('\'admin.advocacy.\'',Route::current()->getName())==1 ?'active':''}}"><a
                        href="{{ route('admin.advocacy.index') }}"><i class='ti-ruler-pencil'></i> <span>Content Authoring</span></a>
            </li>
            <li class="{{preg_match('\'admin.partners.\'',Route::current()->getName())==1 ?'active':''}}"><a
                        href="{{ route('admin.partners.index') }}"><i class='ti-briefcase'></i> <span>Partners</span></a>
            </li>
            <li class="{{preg_match('\'admin.surveys.\'',Route::current()->getName())==1 ?'active':''}}"><a
                        href="{{ route('admin.surveys.index') }}"><i class='ti-write'></i> <span>Surveys</span></a>
            </li>
            <li class="treeview">
                <a href="#"><i class='ti-ink-pen'></i> <span>Blood Bank</span><i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    {{--<li><a href="{{route('performance-reviews.definitions.add') }}"><i class='ti-pencil-alt'></i> <span>New Definition</span></a>--}}
                    {{--</li>--}}
                    {{--<li><a href="{{route('performance-reviews.definitions.view') }}"><i class='ti-eye'></i> <span>View Definition</span></a>--}}
                    {{--</li>--}}
                    {{--<li><a href="{{ route('performance-reviews.index') }}"><i class='fa fa-list'></i>--}}
                    {{--<span>All Reviews</span></a></li>--}}
                </ul>
            </li>
            {{--<li class="treeview">--}}
            {{--<a href="#"><i class='ti-write'></i> <span>Trainings</span><i--}}
            {{--class="fa fa-angle-left pull-right"></i></a>--}}
            {{--<ul class="treeview-menu">--}}
            {{--<li><a href="{{route('trainings.add') }}"><i class='fa fa-plus'></i> <span>New Training</span></a>--}}
            {{--</li>--}}
            {{--<li><a href="{{ route('trainings.index') }}"><i class='fa fa-list'></i>--}}
            {{--<span>All Trainings</span></a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}

            <li><a href="#" data-toggle="control-sidebar"><i class='ti-settings'></i> <span>Settings</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
