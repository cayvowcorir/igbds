<!-- Main Header -->
<header class="main-header" style="background-color:transparent" v-cloak>

    <!-- Logo -->
    <a style="background-color: #F44336;  position: fixed;" href="{{ route('admin.dashboard') }}"  class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span style="color: darkslategray; font-family: 'Julius Sans One', Sans-Serif;" >KNBTS</span> <span style="color: white;font-family: 'Julius Sans One', Sans-Serif;"> | Partner</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation" style="background-color:transparent; color: black">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle  menu-open-button" style="font-size: 18px;padding: 8px 13px;"
           data-toggle="offcanvas">


            {{--<span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>--}}
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu" >
            <ul class="nav navbar-nav dashboard-menu" style="background-color:transparent">
                <li >
                    <div style="width:276px;">
                        <form action="#" method="get" class="topbar-form " style="border: 1px solid darkgrey">
                            <div class="input-group">
                                <input type="text" name="q" class="form-control"
                                       placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
                                <span class="input-group-btn">
                                <button type='submit' name='search' id='search-btn' class="btn btn-flat">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                        </form>
                        <!-- Menu toggle button -->

                    </div>


                </li><!-- /.messages-menu -->
                <li class="dropdown messages-menu" :class="animateWobble==true?'wobble-timed':''">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">@{{ messages.length }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li v-if="messages.length>0">
                            <ul class="menu">
                                <li v-for="message in messages"><!-- start message -->
                                    @php
                                        $route=route('admin.messages.index')
                                    @endphp
                                <a :href="'{{$route}}'+'/'+message.id">
                                <div class="pull-left">
                                <!-- User Image -->
                                <img src=" {{ asset('img/message.png') }}" class="img-circle"
                                alt="User Image"/>
                                </div>
                                <!-- Message title and timestamp -->
                                <h4>
                                @{{ message.subject }}
                                </h4>
                                <!-- The message -->
                                <p>@{{ message.short_description }}</p>
                                    <small><i class="fa fa-clock-o"></i> @{{message.created_at}}</small>
                                </a>
                                </li><!-- end message -->
                            </ul><!-- /.menu -->
                        </li>
                        <li v-else class="header">You have no new messages</li>


                        {{--<li class="footer"><a href="#">c</a></li>--}}
                    </ul>
                </li>
                {{--@php--}}
                    {{--$notifications=\App\Models\Notification::where([--}}
                        {{--'user_id'=> \Cartalyst\Sentinel\Laravel\Facades\Sentinel::getUser()->id,--}}
                        {{--'status'=>'UNREAD'])->get();--}}
                {{--@endphp--}}

                {{--<li class="dropdown notifications-menu {{$notifications->count()>0?'wobble-timed':''}}">--}}


                    {{--<!-- Menu toggle button -->--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                        {{--<i class="fa fa-bell-o"></i>--}}
                        {{--<span class="label label-warning">{{$notifications->count()}}</span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="header">You have {{ $notifications->count()}} unread notifications</li>--}}
                        {{--<li>--}}
                            {{--<!-- Inner Menu: contains the notifications -->--}}

                            {{--<ul class="menu">--}}
                                {{--@foreach($notifications as $notification)--}}
                                {{--<li><!-- start message -->--}}
                                    {{--<a href="{{$notification->link}}">--}}
                                        {{--<div class="pull-left">--}}
                                            {{--<!-- User Image -->--}}
                                            {{--<img src=" {{ asset('img/bell.png') }}" class="img-circle"--}}
                                                 {{--alt="User Image"/>--}}
                                        {{--</div>--}}
                                        {{--<!-- Message title and timestamp -->--}}
                                        {{--<h4>--}}
                                            {{--{{$notification->title}}--}}
                                            {{--<small><i class="fa fa-clock-o"></i> 5 mins</small>--}}
                                        {{--</h4>--}}
                                        {{--<!-- The message -->--}}
                                        {{--<p>{{ $notification->message}}</p>--}}
                                    {{--</a>--}}
                                {{--</li><!-- end message -->--}}
                                    {{--@endforeach--}}
                            {{--</ul><!-- /.menu -->--}}
                        {{--</li>--}}
                        {{--<li class="footer"><a href="#">{{ trans('adminlte_lang::message.viewall') }}</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                <!-- Tasks Menu -->
                <li class="dropdown tasks-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-danger">0</span>
                    </a>
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="header">{{ trans('adminlte_lang::message.tasks') }}</li>--}}
                        {{--<li>--}}
                            {{--<!-- Inner menu: contains the tasks -->--}}
                            {{--<ul class="menu">--}}
                                {{--<li><!-- Task item -->--}}
                                    {{--<a href="#">--}}
                                        {{--<!-- Task title and progress text -->--}}
                                        {{--<h3>--}}
                                            {{--{{ trans('adminlte_lang::message.tasks') }}--}}
                                            {{--<small class="pull-right">20%</small>--}}
                                        {{--</h3>--}}
                                        {{--<!-- The progress bar -->--}}
                                        {{--<div class="progress xs">--}}
                                            {{--<!-- Change the css width attribute to simulate progress -->--}}
                                            {{--<div class="progress-bar progress-bar-aqua" style="width: 20%"--}}
                                                 {{--role="progressbar" aria-valuenow="20" aria-valuemin="0"--}}
                                                 {{--aria-valuemax="100">--}}
                                                {{--<span class="sr-only">20% {{ trans('adminlte_lang::message.complete') }}</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li><!-- end task item -->--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li class="footer">--}}
                            {{--<a href="#">{{ trans('adminlte_lang::message.alltasks') }}</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </li>
                @if ($user=Sentinel::check())
                    <li class="dropdown user user-menu" id="user_menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src=" {{ asset('img/avatar2.png') }}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ $user->first_name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src=" {{ asset('img/avatar2.png') }}" class="img-circle" alt="User Image"/>
                                <p>
                                    {{ $user->first_name }}
                                    <small>{{ $user->last_name }} </small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            {{--<li class="user-body">--}}
                                {{--<div class="col-xs-4 text-center">--}}
                                    {{--<a href="#">{{ trans('adminlte_lang::message.followers') }}</a>--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-4 text-center">--}}
                                    {{--<a href="#">{{ trans('adminlte_lang::message.sales') }}</a>--}}
                                {{--</div>--}}
                                {{--<div class="col-xs-4 text-center">--}}
                                    {{--<a href="#">{{ trans('adminlte_lang::message.friends') }}</a>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ url('/user/profile') }}"
                                       class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.profile') }}</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ trans('adminlte_lang::message.signout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>

                                </div>
                            </li>
                        </ul>
                    </li>
            @else
                <!-- User Account Menu -->

            @endif

            <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
