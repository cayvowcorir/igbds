<?php

namespace Modules\Admin\Http\Controllers;


use App\Models\Survey;
use Debugbar;
use Illuminate\Routing\Controller;
use JsonRPC\Client;
use Illuminate\Http\Request;



define( 'LS_BASEURL', 'https://igbds.limequery.com/');  // adjust this one to your actual LimeSurvey URL
define( 'LS_USER', 'cayvowcorir' );
define( 'LS_PASSWORD', 'kevmusician1994' );

class SurveyController extends Controller
{
    public function allSurveys(){
        $myJSONRPCClient=new \org\jsonrpcphp\JsonRPCClient( LS_BASEURL.'admin/remotecontrol' );
        $session_key=$myJSONRPCClient->get_session_key( LS_USER, LS_PASSWORD );
        $surveys=$myJSONRPCClient->list_surveys($session_key);
        return view('admin::allSurveys', ['surveys'=>$surveys]);
    }

    public function addSurvey(Request $request)
    {
        if ($request->isMethod('GET')) {
            return view('admin::addSurvey');
        }
        else if($request->isMethod('POST')){
            $myJSONRPCClient=new \org\jsonrpcphp\JsonRPCClient( LS_BASEURL.'admin/remotecontrol' );
            $session_key=$myJSONRPCClient->get_session_key( LS_USER, LS_PASSWORD );
            $survey=Survey::create([
                'survey_name'=>$request->title
            ]);
            $response=$myJSONRPCClient->add_survey($session_key,$survey->id,$request->title,'en' );
            Debugbar::info(is_integer($response));
            if(is_integer($response)){
                return \response()->json(['status' => 'success', "message" => ""]);
            }
            else return \response()->json(['status' => 'failed', "message" => $response]);
        }

    }
    public function addSurveyQuestionGroup(Request $request)
    {
       if($request->isMethod('POST')){
            $myJSONRPCClient=new \org\jsonrpcphp\JsonRPCClient( LS_BASEURL.'admin/remotecontrol' );
            $session_key=$myJSONRPCClient->get_session_key( LS_USER, LS_PASSWORD );
            $response=$myJSONRPCClient->list_surveys($session_key);
            Debugbar::info($request->all());
        }

    }
    public function addSurveyQuestions(Request $request)
    {
        if($request->isMethod('POST')){
            $myJSONRPCClient=new \org\jsonrpcphp\JsonRPCClient( LS_BASEURL.'admin/remotecontrol' );
            $session_key=$myJSONRPCClient->get_session_key( LS_USER, LS_PASSWORD );
            $response=$myJSONRPCClient->list_surveys($session_key);
            Debugbar::info($request->all());
        }

    }


}
