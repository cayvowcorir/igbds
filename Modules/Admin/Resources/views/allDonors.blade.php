@extends('admin::layouts.app')

@section('subtitle')
    Donors
@endsection

@section('contentheader_title')
    Donors
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-red ">
                    <div class="box-header with-border">
                        <div class="box-title-wrapper" >
                            <h3 class="box-title light-font">Donors</h3>
                        </div>
                        <div class="box-tools pull-right">

                            <add-donor :add-donors-route="{{json_encode(route('admin.donors.index'))}}"></add-donor>
                        </div>
                        <!-- /.box-tools -->

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-responsive" style="width: 100%; font-size: 14px;" id="donors-datatable">
                            <thead>
                            <tr>
                                <th >First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Blood Group</th>

                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
    <div class="modal-custom"></div>

@endsection

<script>

    @push('jquery-scripts')

      $('#donors-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.donors.data') !!}',
        columns: [
            { data: 'first_name', name: 'first_name'},
            {data: 'last_name', name: 'last_name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'blood_group', name: 'blood_group'},
        ]
    });
    @endpush
</script>
