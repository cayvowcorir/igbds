<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);
        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Donor',
            'slug' => 'donor',
        ]);
        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Partner',
            'slug' => 'partner',
        ]);

        //create user and attach a role
        $credentials = [
            'email'    => 'kevinkorir94@gmail.com',
            'password' => 'password',
            'first_name' => 'KNBTS',
            'last_name' => 'Admin',
            'phone'=>'0710981830'
        ];

        $user = Sentinel::registerAndActivate($credentials);
        $role = Sentinel::findRoleByName('Admin');

        $role->users()->attach($user);

    }
}
