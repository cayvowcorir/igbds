<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace'=>'Modules\Admin\Http\Controllers'], function (){
    //Intercept all options requests
    Route::options('{any}/{route}', function($any, $route)
    {

    })->where('any', '.*')->where('route', '.*');


    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('/reset-password', 'AuthController@resetPassword');
    Route::get('/preview/{fileName}', 'AdvocacyMaterialController@previewAttachment');
    Route::post('/save-token', function (Request $request){
        try{
            if(\App\Models\FcmToken::where('user_id',$request->user_id)->first()!=null){
                \App\Models\FcmToken::where('user_id', $request->user_id)->update(['fcm_token'=>$request->fcm_token]);
            }
            else{
                \App\Models\FcmToken::create([
                    'user_id'=>$request->user_id,
                    'fcm_token'=>$request->fcm_token,
                ]);
            }
            return \response()->json(['status' => 'success', "message" => "FCM Token saved"]);
        }
        catch(\Exception $e){
            return \response()->json(['status' => 'failed', "message" => $e]);
        }



    });
    Route::options('register', function (){return response('');})->middleware('cors');

    Route::options('login', function (){return response('');})->middleware('cors');
    Route::options('donation-centers', function (){return response('');})->middleware('cors');
    Route::options('/user-details', function (){return response('');})->middleware('cors');
    Route::options('/advocacy-material', function (){return response('');})->middleware('cors');
    Route::options('/donation-center/{id}',function (){return response('');})->middleware('cors');
    Route::options('/feedback',function (){return response('');})->middleware('cors');
    Route::options('/save-token',function (){return response('');})->middleware('cors');
    Route::options('/all-users',function (){return response('');})->middleware('cors');
    Route::options('/send-message',function (){return response('');})->middleware('cors');
    Route::options('/user/{id}/chat-messages',function (){return response('');})->middleware('cors');
    Route::options('/user/{id}',function (){return response('');})->middleware('cors');

    Route::group(['middleware'=>['jwt.auth']], function () {
        Route::post('/user-location/update', 'DonorController@updateLocation');
        Route::match(['POST', 'GET'],'/user-details', 'DonorController@profile');
        Route::get('/advocacy-material', 'AdvocacyMaterialController@advocacyMaterial');
        Route::get('/donation-centers', 'DonationCenterController@donationCenters');
        Route::post('/feedback', 'DonorController@feedback');
        Route::post('/send-message', 'ChatController@sendMessage');
        Route::get('/all-users', 'AdminController@allUsers');
        Route::match(['GET', 'POST'],'/user/{id}/chat-messages', 'ChatController@chatMessages');
        Route::get('/donation-center/{id}', 'DonationCenterController@viewDonationCenter');
        Route::get('/user/{id}', 'ChatController@getUserDetails');



    });
});
