@extends('admin::layouts.app')

@section('subtitle')
    View Donor
@endsection

@section('contentheader_title')
    View Donor
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-gray ">

                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="box box-widget widget-user">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header " style="background-color: #EF9A9A">
                                <h3 class="widget-user-username " style="color: white">{{$donor->first_name}} {{$donor->last_name}}</h3>
                                <h5 class="widget-user-desc" style="color: white">(Donor)</h5>
                            </div>
                            <div class="widget-user-image">
                                <img class="img-circle" src="{{asset('/img/avatar5.png')}}" alt="User Avatar">
                            </div>
                            <div class="box-footer" >
                                <div class="row">
                                    <div class="col-sm-4 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header">{{count($bloodDonations)}}</h5>
                                            <span class="description-text">Blood Donations</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header">{{$uniqueDonations}}</h5>
                                            <span class="description-text">Donation Centers Visited</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                        <div class="description-block">
                                            <h5 class="description-header">{{$donor->last_login}}</h5>
                                            <span class="description-text">Last Login</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6" >
                                <div class="box">
                                    <div class="box-header ">
                                        <div class="box-title-wrapper" >
                                            <h3 class="box-title light-font">User Details</h3>
                                        </div>

                                        <!-- /.box-tools -->

                                    </div>
                                    <div class="box-body" style="height: 213px; overflow-y: auto; overflow-x: auto">
                                        <dl class="dl-horizontal">
                                            <dt>First Name</dt>
                                            <dd>{{$donor->first_name}}</dd>
                                            <dt>Last Name</dt>
                                            <dd>{{$donor->last_name}}</dd>
                                            <dt>Phone</dt>
                                            <dd>{{$donor->phone}}</dd>
                                            <dt>Email</dt>
                                            <dd>{{$donor->email}}</dd>
                                            <dt>Blood Group</dt>
                                            <dd>{{$bloodGroup?$bloodGroup->blood_group:'Not Recorded'}}</dd>
                                            <dt>Location</dt>
                                            <dd>{{$userLocations->lat}}, {{$userLocations->lat}}</dd>
                                        </dl>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="box">
                                    <div class="box-header ">
                                        <div class="box-title-wrapper" >
                                            <h3 class="box-title light-font">Donation History</h3>
                                        </div>
                                    </div>
                                    <div class="box-body"  style="height: 213px; overflow-y: auto">
                                        @if(count($bloodDonations)>0)
                                        <table class="table table-responsive" style="width: 100%; font-size: 14px;" id="donors-datatable">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Donation Center</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($bloodDonations as $history)
                                                <tr>
                                                    <td>{{$history->donation_date}}</td>
                                                    <td>{{$history->donationCenter->name}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                            @else
                                            <div class="row" style="text-align: center">
                                                <p>No Data Available</p>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>



                </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
    <div class="modal-custom"></div>

@endsection

<script>


</script>
