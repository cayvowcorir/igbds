@extends('admin::layouts.app')

@section('subtitle')
    Add Surveys
@endsection

@section('contentheader_title')
    Add Surveys
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-red ">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <add-survey :add-survey-route="{{json_encode(route('admin.surveys.add'))}}"></add-survey>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>

@endsection

<script>


</script>
