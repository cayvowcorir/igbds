@component('mail::message')
# Account Activation

Dear Partner,

An IGBDS Partner Account has been created for you by the Kenya National Blood Transfusion Service Admin.
Please click on the link below to create a password for your account and log in. After creating a password, you can log in
to your account on your subsequent accesses at http://partner.igbds.com/auth/login
@component('mail::button', ['url' => route('admin.partners.activate').'?code='.$activationCode->code.'&hash='.$hash])
Click to access your partner account
@endcomponent

Regards,<br>
IGBDS KNBTS Team
{{ config('app.name') }}
@endcomponent
