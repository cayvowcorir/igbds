<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DonationCenterLocation extends Model
{
    protected $fillable=[
        'name',
        'donation_center_id',
        'lat',
        'lng',
    ];
    public function donationCenter(){
        return $this->belongsTo('App\Models\DonationCenter');
    }
}
