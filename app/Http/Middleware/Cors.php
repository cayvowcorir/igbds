<?php

namespace App\Http\Middleware;

use Closure;

class Cors {
    public function handle($request, Closure $next)
    {

        $response= $next($request);
        $response->headers->add(['Access-Control-Allow-Origin'=> '*',
            'Access-Control-Allow-Methods'=>'GET, POST, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers'=>  'Content-Type, X-REQUESTED-BY, Accept, Origin, Authorization'
        ]);

        return $response;
    }
}