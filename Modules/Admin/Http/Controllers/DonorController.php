<?php

namespace Modules\Admin\Http\Controllers;

use App\Events\NewMessage;
use App\Models\BloodDonation;
use App\Models\DonationCenter;
use App\Models\DonationCenterLocation;
use App\Models\Feedback;
use App\Models\UserDetail;
use App\Models\UserLocation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use PHPUnit\Util\RegularExpressionTest;
use Yajra\DataTables\Facades\DataTables;
use Debugbar;
use App\Events\DonorLocationUpdated;

class DonorController extends Controller
{
    public function donors(Request $request){
        if($request->isMethod('GET')){
            return view('admin::allDonors');
        }
        else if ($request->isMethod('POST')){
            $donor=Sentinel::register([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'phone'=>$request->phone,
                'email'=>$request->email,
                'password'=>'password',
            ]);
            $role = Sentinel::findRoleByName('Donor');

            $role->users()->attach($donor);

            UserDetail::create([
                'blood_group'=>$request->blood_group,
                'user_id'=>$donor->id,
            ]);
            return \response()->json(['status'=>'success', "message"=>""]);
        }

    }
    public function donorsData(Request $request)
    {
        $users=Sentinel::getUserRepository()->with('roles')->get();
        $donors=[];
        foreach ($users as $user){
            foreach ($user->roles as $role){
                if($role->name=='Donor') {
                    try {
                        $user->setAttribute('blood_group', UserDetail::where('user_id', $user->id)->first()->blood_group);
                    }
                    catch (\Exception $e){
                        $user->setAttribute('blood_group', 'Not Recorded');
                    }
                    array_push($donors, $user);
                    break;
                }
            }
        }
        return DataTables::of($donors)->make(true);
    }
    //Update User's Location
    public function updateLocation(Request $request){
        try{
            $previousLocation=UserLocation::where('user_id', $request->user->id)->first();
            $donorLocation=null;
            if($previousLocation==null){
                $donorLocation=UserLocation::create([
                    'user_id'=>$request->user->id,
                    'lat'=>$request->lat,
                    'lng'=>$request->lng,
                ]);
            }
            else{
                $donorLocation=UserLocation::where('user_id', $request->user->id)->update(['lat'=>$request->lat, 'lng'=>$request->lng]);
            }
            $location=UserLocation::where('user_id', $request->user->id)->first();
            try{
                broadcast(new DonorLocationUpdated(['user_id'=>$location->user_id, 'lat'=>$location->lat,'lng'=>$location->lng]));
            }
            catch (\Exception $e){
                return \response()->json(['status'=>'failed', "message"=>$e]);
            }

            return \response()->json(['status'=>'success', "message"=>""]);
        }
        catch (\Exception $e){
            return \response()->json(['status'=>'failed', "message"=>$e]);
        }

    }

    public function viewDonor(Request $request)
    {
        if ($request->isMethod('GET')) {
            $donor=Sentinel::findById($request->id);
            $bloodDonations=BloodDonation::where('user_id', $donor->id)->with('donationCenter')->get();
            $uniqueDonations=BloodDonation::where('user_id', $donor->id)->groupBy('donation_center_id')->count();
            $bloodGroup=UserDetail::where('user_id',$donor->id)->first();
            $location=UserLocation::where('user_id',$donor->id)->first();
            return view('admin::viewDonor', ['donor'=>$donor, 'bloodDonations'=>$bloodDonations, 'uniqueDonations'=>$uniqueDonations,
                'userLocations'=>$location,'bloodGroup'=>$bloodGroup]);
        }

    }

    public function profile(Request $request)
    {
        if ($request->isMethod('GET')) {
            $bloodgroup=UserDetail::where('user_id', $request->user->id)->first();
            return \response()->json(['status'=>'success', "user"=>$request->user, "bloodGroup"=>$bloodgroup]);
        }
        else if($request->isMethod('POST')){
            $user=Sentinel::findById($request->user->id);
            $credentials = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'phone' => $request->phone,
            ];
            try{
                $user=Sentinel::update($user, $credentials);
                return \response()->json(['status'=>'success', "user"=>$user]);
            }
            catch (\Exception $e){
                return \response()->json(['status'=>'failed', "message"=>$e]);

            }

        }

    }

    public function feedback(Request $request)
    {
        if ($request->isMethod('POST')) {
            try{
                $feedback=Feedback::create([
                    'subject'=>$request->subject,
                    'short_description'=>$request->short_description,
                    'detailed_description'=>$request->detailed_description,
                    'user_id'=>$request->user->id
                ]);
                $userDetails=Sentinel::findById($request->user->id);
                broadcast(new NewMessage(['subject'=>$request->subject, 'short_description'=>$request->short_description,
                    'email'=>$userDetails->email, 'time'=>$feedback->created_at]));

                return \response()->json(['status' => 'success', "message" => ""]);
            }
            catch (\Exception $e){
                return \response()->json(['status' => 'failed', "message" => $e]);
            }

        }

    }
}
