<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => 'AAAA2nA8z5k:APA91bHLrwICLZDizcjsfdog4y-yLt9jwms3d_F0FH_B7zOftQ_JHx_Twr_QBmbK10c6FK7YdGLshDqByuCVw25PIqbB2bgfB876x0o8iPJDhjBVpK6AEi-YZ9mXisN3zdILqOT6NlVK',
        'sender_id' => '938185904025',
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
