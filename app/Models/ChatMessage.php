<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    protected $fillable=[
      'recipient_id',
      'user_id',
      'message',
      'status',
    ];
    public function recipient(){
        return $this->belongsTo('App\Models\Sentinel\SentinelUserModel', 'recipient_id');
    }
    public function sender(){
        return $this->belongsTo('App\Models\Sentinel\SentinelUserModel', 'user_id');
    }
}
