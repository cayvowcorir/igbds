<!-- Control Sidebar -->
<aside style="overflow-y: scroll; height: 100vh" class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Organization Setup</h3>
            <ul class='control-sidebar-menu'>
                <li>
                    @php
                        $percentage=0;

                    @endphp
                    @if($percentage>80)
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                            Percentage Complete:
                            <span class="label label-success pull-right">{{$percentage}}%</span>
                        </h4>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" style="width: {{$percentage}}%"></div>
                        </div>
                    </a>
                        @else
                        {{--<a href='{{route('organization.modify')}}'>--}}
                            {{--<h4 class="control-sidebar-subheading">--}}
                                {{--Percentage Complete:--}}
                                {{--<span class="label label-danger pull-right">{{$percentage}}%</span>--}}
                            {{--</h4>--}}
                            {{--<div class="progress progress-xxs">--}}
                                {{--<div class="progress-bar progress-bar-danger" style="width: {{$percentage}}%"></div>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                        @endif
                </li>
            </ul>
            <h3 class="control-sidebar-heading">Organization Settings</h3>
            <ul class='control-sidebar-menu'>

               <li>
                    {{--<a href='{{ route('miscellaneous') }}'>--}}
                        {{--<i class="menu-icon ti-pulse bg-green"></i>--}}
                        {{--<div class="menu-info">--}}
                            {{--<h4 class="control-sidebar-subheading">Miscellaneous</h4>--}}
                            {{--<p>Adjust the Benefits accorded to Employees</p>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                </li>

            </ul><!-- /.control-sidebar-menu -->


            <!-- /.control-sidebar-menu -->

        </div><!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">{{ trans('adminlte_lang::message.statstab') }}</div><!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">{{ trans('adminlte_lang::message.generalset') }}</h3>
                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        {{ trans('adminlte_lang::message.reportpanel') }}
                        <input type="checkbox" class="pull-right" {{ trans('adminlte_lang::message.checked') }} />
                    </label>
                    <p>
                        {{ trans('adminlte_lang::message.informationsettings') }}
                    </p>
                </div><!-- /.form-group -->
            </form>
        </div><!-- /.tab-pane -->
    </div>
</aside><!-- /.control-sidebar

<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
<div class='control-sidebar-bg'></div>