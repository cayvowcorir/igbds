@extends('admin::layouts.app')

@section('subtitle')
    View Message
@endsection

@section('contentheader_title')
    View Message
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-6">

                <div class="box box-gray ">
                    <div class="box-header with-border">
                        <div class="box-title-wrapper" >
                            <h3 class="box-title light-font">Chat</h3>
                        </div>
                        <div class="box-tools pull-right">


                        </div>
                        <!-- /.box-tools -->

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <view-message :chat-route="{{json_encode(\Illuminate\Support\Facades\Request::url())}}" :messages="{{json_encode($messages)}}"></view-message>

                    </div>



                </div>



            </div>
            <div class="col-sm-6">

                <div class="box box-gray ">
                    <div class="box-header with-border">
                        <div class="box-title-wrapper" >
                            <h3 class="box-title light-font">User Details</h3>
                        </div>
                        <div class="box-tools pull-right">


                        </div>
                        <!-- /.box-tools -->

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box box-widget widget-user">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header" style="background-color: #F44336">
                                <h3 class="widget-user-username " style="color: white">{{$recipientDetails->first_name}} {{$recipientDetails->last_name}}</h3>
                                <h5 class="widget-user-desc" style="color: white">Donor</h5>
                            </div>
                            <div class="widget-user-image">
                                <img class="img-circle" src="{{asset('images/avatar.png')}}" alt="User Avatar">
                            </div>
                            <div class="box-footer" style="font-size: 13px">
                                <div class="row">
                                    <div class="col-sm-4 border-right">
                                        <div class="description-block">
                                            <h6 style="font-size: 90%" class="description-header">{{count($bloodDonations)}}</h6>
                                            <span class="description-text">Blood Donations</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 border-right">
                                        <div class="description-block">
                                            <h6 style="font-size: 90%" class="description-header">{{$uniqueDonations}}</h6>
                                            <span class="description-text">Donation Centers Visited</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                        <div class="description-block">
                                            <h6 style="font-size: 90%" class="description-header">{{$recipientDetails->last_login}}</h6>
                                            <span class="description-text">Last Login</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                        </div>

                    </div>



                </div>



            </div>
        </div>
    </div>

@endsection

<script>


</script>
