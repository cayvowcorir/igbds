<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvocacyMaterial extends Model
{
    protected $fillable=[
      'name',
        'description',
        'attachment_url',
        'preview_url',
        'author_id'
    ];
}
