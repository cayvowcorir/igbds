<?php

namespace App\Models\Sentinel;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class SentinelUserModel extends \Cartalyst\Sentinel\Users\EloquentUser
{
    use Searchable;
    protected $fillable = [
        'email',
        'last_name',
        'first_name',
        'phone',
        'password'
    ];
    public function donorDetails(){
        return $this->hasOne('App\Models\UserDetail', 'user_id');
    }


    public $asYouType = true;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,

        ];
    }

}