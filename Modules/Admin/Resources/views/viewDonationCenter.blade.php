@extends('admin::layouts.app')

@section('subtitle')
    View Donation Center
@endsection

@section('contentheader_title')
    View Donation Center
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-gray ">

                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="box box-widget widget-user">
                            <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header " style="background-color: #EF9A9A">
                                <h3 class="widget-user-username " style="color: white">{{$donationCenter->name}}</h3>
                                <h5 class="widget-user-desc" style="color: white">{{$donationCenter->description}}</h5>
                            </div>
                            <div class="widget-user-image">
                                <img class="img-circle" src="{{asset('/img/avatar.png')}}" alt="User Avatar">
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-4 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header">{{count($allDonations)}}</h5>
                                            <span class="description-text">Blood Donations Received</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header">{{$disbursedDonations}}</h5>
                                            <span class="description-text">Donations Disbursed</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                        <div class="description-block">
                                            <h5 class="description-header"></h5>
                                            <span class="description-text">Last Login</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6" >
                                <div class="box">
                                    <div class="box-header ">
                                        <div class="box-title-wrapper" >
                                            <h3 class="box-title light-font">Donation Center Details</h3>
                                        </div>

                                        <!-- /.box-tools -->

                                    </div>
                                    <div class="box-body" style="height: 263px; overflow-y: auto; overflow-x: auto">
                                        <dl class="dl-horizontal">
                                            <dt>Name</dt>
                                            <dd>{{$donationCenter->name}}</dd>
                                            <dt>Description</dt>
                                            <dd>{{$donationCenter->description}}</dd>
                                            <dt>Physical Address</dt>
                                            <dd>{{$donationCenter->physical_address}}</dd>
                                            <dt>Email</dt>
                                            <dd>{{$donationCenter->email}}</dd>
                                            <dt>Phone</dt>
                                            <dd>{{$donationCenter->phone}}</dd>
                                            <dt>Location</dt>
                                            <dd>{{$donationCenter->donationCenterLocation->lat}}, {{$donationCenter->donationCenterLocation->lat}}</dd>
                                        </dl>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6">

                                <div class="box">
                                    <div class="box-header ">
                                        <div class="box-title-wrapper" >
                                            <h3 class="box-title light-font">Disbursements</h3>
                                        </div>

                                        <!-- /.box-tools -->

                                    </div>
                                    <div class="box-body"  style="height: 263px; overflow-y: auto">
                                      <disbursement-history :blood-donations="{{$allDonations}}" :disbursed-donations="{{$disbursedDonations}}"></disbursement-history>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>



                </div>
                    <!-- /.box-body -->
                </div>

            </div>
        <div class="row">
            <div class="col-sm-12">
                <donation-history :donation-history="{{$aggregatedBloodDonations}}"></donation-history>
            </div>
        </div>
        </div>
    <div class="modal-custom"></div>

@endsection

<script>


</script>
