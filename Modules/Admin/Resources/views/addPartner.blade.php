@extends('admin::layouts.app')

@section('subtitle')
    Add Surveys
@endsection

@section('contentheader_title')
    Add Surveys
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-red ">
                    <!-- /.box-header -->
                    <div class="box-title-wrapper" >
                        <h3 class="box-title light-font">Add Partner</h3>
                    </div>
                    <div class="box-body">
                        <add-partner :add-partners-route="{{json_encode(route('admin.partners.add'))}}"></add-partner>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>

@endsection

<script>


</script>
