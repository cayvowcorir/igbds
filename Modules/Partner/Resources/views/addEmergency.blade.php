@extends('admin::layouts.app')

@section('subtitle')
    Report Emergency
@endsection

@section('contentheader_title')
    Report Emergency
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-red ">
                    <!-- /.box-header -->
                    <div class="box-title-wrapper" >
                        <h3 class="box-title light-font">Report Emergency</h3>
                    </div>
                    <div class="box-body">
                        <report-emergency :donation-centers="{{$donationCenters}}"></report-emergency>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>

@endsection

<script>


</script>
