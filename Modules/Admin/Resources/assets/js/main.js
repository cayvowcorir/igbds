
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import VueFormWizard from 'vue-form-wizard'
import * as VueGoogleMaps from 'vue2-google-maps'
import Element from 'element-ui'
import Vue from 'vue';
import VueHighcharts from 'vue-highcharts';
import loadSolidGauge from 'highcharts/modules/solid-gauge';
import Highcharts from 'highcharts';
import VueLazyload from 'vue-lazyload'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import '../../../../../public/vendor/elementui-custom-theme/theme/index.css';
import "../../../../../node_modules/vue-wysiwyg/dist/vueWysiwyg.css";
import locale from 'element-ui/lib/locale/lang/en';
import wysiwyg from "vue-wysiwyg";

loadSolidGauge(Highcharts);


require('./bootstrap');

window.Vue = require('vue');



Vue.use(VueFormWizard)
Vue.use(Element, { locale})
Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyB_ppML-oy_aQT7-iZ4prcDffyE65TPi74",
        libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)
    }
});
Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: 'images/error.png',
    loading: 'images/loading.svg',
    attempt: 1,
    lazyComponent: true
})
Vue.use(wysiwyg, {});
Vue.use(VueHighcharts, {Highcharts});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('add-donation-center', require('./components/AddDonationCenter.vue'));
Vue.component('add-donor', require('./components/AddDonor.vue'));
Vue.component('dashboard-map', require('./components/DashboardMap.vue'));
Vue.component('donation-history', require('./components/DonationHistory.vue'));
Vue.component('disbursement-history', require('./components/DisbursementHistory.vue'));
Vue.component('all-advocacy', require('./components/AllAdvocacy.vue'));
Vue.component('surveys', require('./components/Surveys.vue'));
Vue.component('add-survey', require('./components/AddSurvey.vue'));
Vue.component('add-partner', require('./components/AddPartner.vue'));
Vue.component('view-message', require('./components/ViewMessage.vue'));

const app = new Vue({
    el: '#app',
    data(){
        return{
            messages:[],
            animateWobble:false
        }
    },
    created:function () {
        var _this=this;

        // window.Echo.channel('newMessage')
        //     .listen('NewMessage', (e) => {
        //         _this.messages.push(e)
        //     });
        window.Echo.channel('newChatMessage')
            .listen('NewChatMessage', (e) => {
                e.message.created_at=e.message.date.date;
                _this.messages.push(e.message);
            });
        setInterval(function(){
            _this.animateWobble==true?_this.animateWobble=false:_this.animateWobble=true;
        }, 10500)
    },
    mounted: function () {

        this.getMessages();
    },
    methods:{
        getMessages:function () {
            console.log('getting messages');
            var _self=this;
            axios.get('messages').then(function (response) {
                _self.messages=response.data.messages;
                if(_self.messages.length>0){
                    _self.animateWobble=true;
                }
                else _self.animateWobble=false;
            }).catch(function (error) {

            })
        }
    },
});


// setInterval(function () {
//     this.loadData();
// }.bind(this), 30000);

