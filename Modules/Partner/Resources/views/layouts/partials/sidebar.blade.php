<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar sidebar-overlay"
       style="position: fixed;background-image: url({{asset('img/sidebar_bg.jpg')}})">
    <hr style="margin:10px 5px 5px 20px; position: relative; ">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if ($user=Sentinel::check())
            <div class="user-panel" style="margin-left: 10px;">
                <div class="pull-left image">

                    <img src=" {{ asset('img/avatar5.png') }}" class="img-circle" alt="Your Logo"/>

                </div>
                <div class="pull-left info">
                    <p>{{ $user->first_name }}</p>
                {{--<p>User {{$user}}</p>--}}
                <!-- Status -->
                    <a href="#" style=" color: white!important; font-size: 14px"><i
                                class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
        @endif
        <hr style="margin:10px 5px 5px 20px; position: relative; ">

        <!-- search form (Optional) -->

        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">

            <!-- Optionally, you can add icons to the links -->
            <li class="{{preg_match('\'partner.dashboard\'',Route::current()->getName())==1 ?'active':''}}"><a
                        href="{{ route('partner.dashboard') }}"><i class='ti-dashboard'></i> <span>Dashboard</span></a>
            </li>
            <li class="{{preg_match('\'partner.emergency.\'',Route::current()->getName())==1 ?'active':''}}"><a
                        href="{{ route('partner.emergency.add') }}"><i class='fa fa-ambulance'></i> <span>Emergency Requests</span></a>
            </li>
            <li class="{{preg_match('\'partner.donations.\'',Route::current()->getName())==1 ?'active':''}}"><a
                        href="{{ route('partner.donations.add') }}"><i class='fa fa-tint'></i> <span>Record Donation</span></a>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
