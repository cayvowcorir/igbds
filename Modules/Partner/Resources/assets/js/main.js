
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import Autocomplete from 'vue2-autocomplete-js'
import Element from 'element-ui'
import '../../../../../public/vendor/elementui-custom-theme/theme/index.css';
import locale from 'element-ui/lib/locale/lang/en';

Vue.use(Element, { locale})

require('./bootstrap');

window.Vue = require('vue');


Vue.use(Autocomplete);
require('vue2-autocomplete-js/dist/style/vue2-autocomplete.css')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));


Vue.component('reset-password-form', require('./components/auth/ResetPasswordForm.vue'));
Vue.component('record-donation', require('./components/RecordDonation.vue'));
Vue.component('report-emergency', require('./components/ReportEmergency.vue'));

const app = new Vue({
    el: '#app',
    data(){
        return{
messages:[],
            animateWobble:false
        }
    },
    created:function () {

    },
    mounted: function () {


    },
    methods:{

    },
});


// setInterval(function () {
//     this.loadData();
// }.bind(this), 30000);

