@extends('admin::layouts.app')

@section('subtitle')
    Add Donation Center
@endsection

@section('contentheader_title')
    Add Donation Center
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-sm-12">

                <div class="box box-red shadowDepth1 ">
                    <div class="box-header with-border" >
                        <h1 class="box-title light-font">Add Donation Center</h1>
                        <div class="box-tools pull-right">

                        </div>
                        <!-- /.box-tools -->

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <add-donation-center :add-donation-center-route="{{json_encode(route('admin.donation_centers.add'))}}"></add-donation-center>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </div>
@endsection

<script>
</script>
